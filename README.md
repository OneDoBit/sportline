# sportline

## Build Setup

```bash
# install dependencies
$ npm install

# serve with hot reload at localhost:3000
$ npm run dev

# build for production and launch server
$ npm run build
$ npm run start

# generate static project
$ npm run generate
```

https://www.mixamo.com/#/?page=1&query=box&type=Motion%2CMotionPack
https://dribbble.com/shots/13012281-Fitty-Landing-Page?utm_source=Clipboard_Shot&utm_campaign=Izmahsa&utm_content=Fitty%20Landing%20Page&utm_medium=Social_Share
https://dribbble.com/shots/9827016-Template-DJ-mobile
